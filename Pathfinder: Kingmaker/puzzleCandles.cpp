//This program solves the Pathfinder: Kingmaker candle hexagram puzzle.
//Just build and run in a console. The program will display a help message.
//Tested with g++, --std=c++0x, at the very least. Most compilers should have no problem. Does not work with --std=c++98. 

#include<iostream>
#include<vector>
#include<utility>
#include<cstring>

using namespace std;

typedef pair< unsigned short int, vector< unsigned short int > > transition;
vector<transition> g_transitions = {
  { 0, {0, 4} },
  { 1, {1, 3} },
  { 2, {2, 4, 5} },
  { 3, {3, 1, 5} },
  { 4, {4, 0, 2} },
  { 5, {5, 2, 3} }
};

vector<bool> g_stateCurrent = {1, 0, 1, 1, 1, 1}, g_stateTarget = {1, 1, 1, 1, 1, 1};

template <class T>
void printVec(vector<T> vec, bool nl = true) {
  for( auto e: vec )
    cout<<e<<' ';
  if(nl) cout<<endl;
}

inline void applyTransition(vector<bool>& currentState, const transition t) {
  for(int jj = 0; jj < t.second.size(); ++jj)
    currentState[ t.second[jj] ] = !currentState[ t.second[jj] ];
}

inline bool const statesMatch(vector<bool> currentState, vector<bool> targetState) {
  bool match = 1;
  for(int jj = 0; jj < targetState.size(); ++jj)
    if(currentState[jj] != targetState[jj]) { match = 0; break; }
  return match;
}
		 
vector<short int> bf(const vector<bool>& stateCurrent, const vector<bool>& stateTarget, const vector<transition>& transitions) {
  vector<short int> trace;
  vector<short int> tempTrace;
  vector<bool> stateTemp;
  vector<transition> currentTransitions;
  for(int ii = 0; ii < transitions.size(); ++ii) {
    stateTemp = stateCurrent;
    applyTransition(stateTemp, transitions[ii]);
    if(statesMatch(stateTemp, stateTarget) == 1) {
      trace.push_back( transitions[ii].first );
      return trace;
    }
    currentTransitions = transitions;
    currentTransitions.erase(currentTransitions.begin() + ii);
    tempTrace = bf(stateTemp, stateTarget, currentTransitions);
    if(tempTrace.size() != 0) {
      trace.push_back( transitions[ii].first );
      for(int jj = 0; jj < tempTrace.size(); ++jj)
	trace.push_back(tempTrace[jj]);
      return trace;
    }
  }
  return trace;
}

void printUsage() {
  cout<<"This program solves the Pathfinder: Kingmaker candle hexagram puzzle. The purpose of the puzzle is to extinguish all the candles. \n The program receives a single input: the candle current state (0 for unlit, 1 for lit, space-separated), such as 1 1 1 1 1 1. The program outputs which candles you should interact with, and the effect this has. \n The candles are numbered in the following way: using the default camera orientation, candle 0 lies in the first trigonometric quadrant (minimal angle). Candle 1 is the next, trigonometrically. \n That is, imagine the hexagram divided by a horizontal line passing through its centre. Candle 0 is the rightmost candle which is still above the line. Candle 1 is the next, counter-clockwise, and so on:\n   1    \n2     0\n\n3     5\n   4 \n Symbolically, the candle you interact with is followed by a '*', while the candles it also changes are followed by a '-'."<<endl; 
}

void printHexagramTransition(const vector<bool>& state, const transition& t = { -1, {} }) {
  vector<char> change(6, ' ');
  if(t.first >= 0 && t.first < 6) {
    for(int ii = 0; ii < t.second.size(); ++ii) {
      if(t.second[ii] >= 0 && t.second[ii] < 6)
	change[ t.second[ii] ] = '-';
    }
    change[t.first] = '*';
  }
  cout<<"   "<<state[1]<<change[1]<<"   "<<endl<<state[2]<<change[2]<<"    "<<state[0]<<change[0]<<endl<<endl<<state[3]<<change[3]<<"    "<<state[5]<<change[5]<<endl<<"   "<<state[4]<<change[4]<<endl; 
}

int main(int argc, char* argv[]) {
  if(argc == 1) {
    printUsage();
    exit(1);
  }
  if(argc > 1) {
    if( strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0 ) {
      printUsage();
      exit(1);
    }
    g_stateCurrent = {};
    for(int ii = 1; ii < argc; ++ii) {
      if(strcmp(argv[ii], "0") != 0 && strcmp(argv[ii], "1") != 0) { printUsage(); exit(1); }
      g_stateCurrent.push_back( (argv[ii][0] == '0')?0:1 );
    } 
    g_stateTarget = vector<bool>(g_stateCurrent.size(), 0);
  }
  cout<<"Current state: ";
  printVec(g_stateCurrent);
  cout<<"Target  state: ";
  printVec(g_stateTarget);
  cout<<endl;
  if( statesMatch(g_stateCurrent, g_stateTarget) ) {
    cout<<"The states already match."<<endl;
    exit(0);
  }
  vector<short int> trace = bf(g_stateCurrent, g_stateTarget, g_transitions);
  if(trace.size() == 0) cout<<"Could not find a transition sequence from the current state to the target state.";
  else {
    printUsage();
    cout<<"--------------------------------------"<<endl<<endl<<"Transitions: ";
    printVec(trace);
    cout<<endl;
    auto state = g_stateCurrent;
    printVec(state);
    printHexagramTransition(state);
    cout<<endl;
    for(auto &step: trace) {
      for(auto &t: g_transitions) {
	if(step == t.first) {
	  applyTransition(state, t);
	  printVec(state, false);
	  cout<<": Transition "<<t.first<<"->";
	  printVec(t.second);
	  printHexagramTransition(state, t);
	  cout<<endl;	  
	}
      }
    }
  }
  return 0;
}
